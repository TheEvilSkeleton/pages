---
layout: post
title: "Why the Linux Desktop has not yet been Adopted by the Masses"
description: "This is an opinion-based article about why I think the Linux desktop has not yet been adopted by the masses."
toc: true
tags: ["Linux"]
redirect_from: /2021/04/06/why-the-linux-desktop-has-not-yet-been-adopted-by-the-masses.html
---

**If the ad-blocker is enabled, please deactivate the ad-blocker here so that all embeddings are displayed!**

## Introduction
The Linux desktop has been around for more than two decades. It has a very small market share of about 4% (including ChromeOS), and has been for quite a while.<sup>[(Source)](https://gs.statcounter.com/os-market-share/desktop/worldwide/#monthly-202004-202103-bar)</sup>

Most original equipment manufacturers (OEMs) ship Windows 10 on most of their systems by default. Apple is one of the few, if not the only, *commonly* known exception where they do not primarily ship with Windows 10.

Very few OEMs ship their systems primarily with GNU/Linux pre-installed: [System76](https://system76.com/), [Purism](https://puri.sm/), and [Tuxedo](https://www.tuxedocomputers.com/index.php) are the best known. Compared to other OEMs such as Acer, HP, Dell, and others, the organizations that include GNU/Linux primarily are not well known or widely used. Fortunately, Dell has a program for systems that ship with Linux,<sup>[(Source)](https://www.dell.com/en-us/work/shop/overview/cp/linuxsystems)</sup> but only a minority are aware of it.

Also, the education system does not promote Linux desktop, and I can understand that; there is no good support in Linux desktop when it comes to software for education. Microsoft Office 365 (MS O365) has been used for a very long time, and while LibreOffice and OnlyOffice are good free and open source (FOSS) alternatives to MS O365, they are not as feature rich, mature, user friendly and/or familiar as their counterpart.

Unfortunately, the platform was never adopted by the masses, and the Linux desktop was never a competitor to Windows 10 or even macOS. Many people have theorized about this problem, but I want to share mine.

A slight warning that this post is opinionated.

## The Community
This is my theory and the one I subscribe to: **the Linux community is the main reason why the Linux desktop has not yet been adopted by the masses**.

### Lack of Testers and Reporters
It is very common for a person to complain about the instability of a distribution or software, but very rare for them to report the problem(s) to the maintainers. It is also common for a person to wish for a feature, but very rare for them to suggest it in the issue tracker. This is something very important to recognize: Many projects in the Linux desktop generally do *not* track the user like Windows 10 and macOS do, so it is very difficult to collect data from the user to improve the software. Telemetry is used a bit, but it's not enough, as the data passed to upstream is very minimal compared to how much other OSes collect from the user to improve UI, UX, and stability.

**That's why it's better to report bugs, answer surveys, and suggest features than to ignore them.** Investing little time to go to the project's GitHub, GitLab, Bugzilla, etc. and report something is critical to the health of the Linux desktop. A project cannot improve if no one is willing to contribute.

### Lack of Code Contributors, Developers and Maintainers
Maintainers and developers cannot work full time and/or actively on a project for which they are not paid. Software is hard to develop, but even harder to maintain. It is difficult to fix bugs or develop new features when the developer's time is very limited.

**It is important to provide financial support and/or contribute to the code by implementing features, fixing bugs, improving performance, etc.** Without code contribution, progress in projects is very slow, especially in larger projects.

### Toxicity
Unfortunately, the Linux desktop community is very toxic. Wars between fans of desktop environments (DEs), distributions, package managers, package formats, etc., threats, personal attacks, etc. are very common in public chat rooms and forums.

#### GNOME "Vs." KDE/Plasma
GNOME and Plasma are the two most used desktop environments on Linux, as either one or the other is included by default by the major distributions. However, many users are very dismissive and think that one is better than the other. This creates a toxic environment in the Linux community and on the Linux desktop.

GNOME and KDE have different goals in mind in their respective desktop environments. GNOME and Plasma are aimed at different people, for different use cases, and both fulfill them correctly. *But that doesn't make one generally better than the other.*

For some time, both organizations have done the opposite of competing with each other. Rather, they have supported each other:

1. Supporting each other is the key to friendship. It's always good to celebrate progress, achievements, special days, etc. GNOME and KDE congratulate each other on every major release, birthdays, and more:

    <iframe src="https://floss.social/@gnome/101584833590176948/embed" class="mastodon-embed" style="max-width: 100%; border: 0" height="400" allowfullscreen="allowfullscreen"></iframe>
    <iframe src="https://mastodon.technology/@kde/102623685299330645/embed" class="mastodon-embed" style="max-width: 100%; border: 0" height="400" allowfullscreen="allowfullscreen"></iframe>

2. Collaboration is a great way to improve the quality of the relationship. By working together, both parties learn to communicate and work with each other, and eventually improve the quality of the relationship and work efficiency between the two. GNOME and KDE have been working together for more than a decade at conferences and the Linux App Summit:
    - [KDE and GNOME to Co-locate Flagship Conferences on Gran Canaria in 2009](https://ev.kde.org/2008/07/11/kde-and-gnome-to-co-locate-flagship-conferences-on-gran-canaria-in-2009/)
    - [Free Desktop Communities come together at the Gran Canaria Desktop Summit](https://ev.kde.org/2009/08/06/free-desktop-communities-come-together-at-the-gran-canaria-desktop-summit/)
    - [GNOME Foundation and KDE e.V. to Co-Host Conferences in 2011](https://ev.kde.org/2010/03/16/gnome-foundation-and-kde-e-v-to-co-host-conferences-in-2011/)
    - [GNOME and KDE work together on the Linux desktop](https://www.zdnet.com/article/gnome-and-kde-work-together-on-the-linux-desktop/)
3. There are many options on the Linux desktop. Unfortunately, this has led to fragmentation of the Linux desktop, as each option does things differently. Fortunately, GNOME and KDE are working together to standardize the Linux desktop by promoting similar technologies while working independently. Although this sounds unusual, since Plasma uses Qt and GNOME uses GTK, and both DEs are aimed at different people, for different use cases, GNOME and KDE strongly encourage the use of [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal), software that helps an application change dynamically depending on the desktop environment or window manager, such as opening the desktop environment's native file dialog (also known as file picker and file chooser) no matter which toolkit is used, e.g., [GTKFileChooserDialog](https://developer.gnome.org/gtk3/stable/GtkFileChooserDialog.html) when using Qt applications in GNOME and [KFileDialog](https://api.kde.org/pykde-4.2-api/kio/KFileDialog.html) when using GTK applications in Plasma. They also promote "technologies such as libappindicator, Flatpak, Flathub, Wayland, and many, many others."<sup>[(Source)](https://www.reddit.com/r/gnome/comments/dtjbau/gnomekde_cooperation/f6xyct1/?context=3)

It is important to understand that none of them is better. They are aimed at different people and for different use cases. GNOME and KDE, if anything, are practically fixing the Linux desktop together by letting different technologies work together and using the same technologies in many places.

If two "competitors" have been supporting each other for more than a decade, why can't the community do the same? **By supporting, collaborating, and agreeing on a standard, standardization and options without monopolization will greatly improve the Linux desktop.**

#### Package Managers
No system package manager is better. Each package manager is aimed at different people, for different use cases, and they all fulfill them properly. Some are built for speed, some for configurations, some for security, etc. *This doesn't make one package manager better than another in general.*

Unfortunately, since there are many different package managers, it is very difficult to ship applications across different Linux distributions because the developer has to package for different distributions and maintain each package, or the applications have to be shipped unofficially.

This is where Flatpak comes into play. Flatpak is a universal package manager. One of its goals is to standardize the packaging of *applications* on the Linux desktop to ensure official support by the upstream developers themselves. The same application is nearly identical from distribution to distribution, whether using Arch Linux, Debian, Gentoo, Fedora, etc., because the dependencies it ships with are the same versions.

Many developers have ignored the Linux desktop for a very long time, simply because of fragmentation. Flatpak solves this problem by only having to maintain a single package, which enables it to ship that package for almost every Linux distribution, rather than maintaining a separate package for each distribution. Unfortunately, Flatpak requires more space to store files that would normally be on the distribution to make it distribution independent. This is something that much of the Linux community focuses on and considers unforgivable, but ignores all the other benefits that Flatpak offers. Some benefits of Flatpak are:
- Standardized and distribution agnostic implementation of shipped applications.
- Greater chance of official support from the application developer.
- Easier communication between the user and the developer.
- No root privilege required to install applications.

The benefits listed above are explained in a video made by Flatpak's maintainer, Alexander Larsson, [Flatpak, an introduction (Lightning Talk) - Alexander Larsson](https://www.youtube.com/watch?v=0yeXTounX3E).

**By agreeing on a standardized way to deliver applications to the Linux desktop, more developers can consider the Linux desktop as a viable option**. Storage is cheaper than ever before. It is possible to compress most user files and save about 40% of space usage while improving performance by simply switching to btrfs and using zstd.<sup>[(Source)](https://fedoraproject.org/wiki/Changes/BtrfsTransparentCompression)</sup> There are many more ways to reduce space usage. As a side note, while Flatpak may not benefit some users specifically, it benefits everyone collectively: more distributions can support applications, and more developers can easily ship their packages on the Linux desktop, making it a viable option.

#### systemd
Without a doubt, systemd was (and still is) one of the most controversial projects in the Linux desktop. This is because its creator, Lennart Poettering, is known to have many controversial positions on the Linux ecosystem and UNIX philosophy.

The reason this is brought up is the amount of harassment Poettering got/is getting due to his influence on the Linux desktop. He has been personally attacked many times and even *allegedly* received some death threats.<sup>[(Source)](https://www.zdnet.com/article/lennart-poetterings-linus-torvalds-rant/)</sup>

**It's important to give constructive and friendly criticism instead of personally attacking people for doing something.** No one deserves to be bullied, harassed, doxxed, personally attacked, or death threats. If a developer or user wants to feel welcome and contribute to the Linux community, it's best to be kind.

### Conservative Mindset
The mentality of the community is a big factor. "Why do we need 'A' when 'B' works?", "'A' sucks because it doesn't follow the UNIX philosophy", "'A' sucks because it reinvents the wheel" are some quotes people often make to either themselves or the community. "Why do we need Wayland when X11 works?", "systemd sucks because it doesn't follow the UNIX philosophy", "Flatpak sucks because it reinvents the wheel", etc.

**There's nothing wrong with there being room for improvement, there being room for progress, or there being room to fix fundamentally flawed and/or outdated implementations.** The Linux desktop currently works, but it's fragmented: There are too many package managers, package formats, and distributions, but not enough packagers and developers focused on the Linux desktop. X11 works, but it lacks modern implementations like window isolation; it is very slow and it is prone to artifacts and screen tearing. PulseAudio works, but it lacks modern implementations like audio isolation and it is slow compared to macOS and Windows 10. ext4 works, but it lacks proper SSD optimization and it lacks modern features like RAID. The current Linux desktop security model works, but a breach of system packages can cause the current installation to break.

## Conclusion
The lack of games, software, advertising and monetization are only minor problems on the Linux desktop as to why the Linux desktop has not yet been adopted by the masses, but it is not the main reason. The main reason, in my opinion, is the community: complaining about problems but not reporting them, lack of code/documentation contributions, the very toxic behavior and the rejection of progress and improvement. There are many things that need to be fixed in the Linux desktop. Windows 10 and macOS live in 2021, while the Linux desktop is still catching up to 2010 where modernization and standards are lacking. Users often reject the replacements/improvements because everything "works" for them, even though there are many things that desperately need to be improved in order to be adopted by the masses.

---

*Everything works in the Linux desktop, but nothing works well.*
