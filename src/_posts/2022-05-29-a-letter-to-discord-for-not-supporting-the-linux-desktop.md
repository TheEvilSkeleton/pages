---
layout: post
title: "A Letter to Discord for not Supporting the Linux Desktop"
description: "
Discord is popular among the Linux desktop community. Thanks to Electron, the framework that Discord uses, it was possible for Discord to port the client over to Linux very easily. Electron facilitates deploying the same application in different desktop platforms.


However, Electron has one major drawback: it is up to the application developer to update (rebase) to a newer version of Electron to fix security bugs, improve stability and improve compatibility with newer technologies. The Discord client uses an Electron version that is questionably low.


In this article, we're going to look at PipeWire and Wayland compatibility, specifically the lack thereof; and the potential security risk with their practices.
"
toc: true
tags: ["Linux", "Flatpak", "Wayland"]
disclaimer: "This post is not meant to attack the Discord developers, rather to write in hope that Discord will listen to the Linux community about practical issues with the Discord client on the Linux desktop. I also haven't contacted the Discord developers, so information may be wrong."
redirect_from: /2022/05/29/a-letter-to-discord-for-not-supporting-the-linux-desktop.html
---

## Introduction

{{ page.description }}

## Announcement
At the time of updating this article, Discord updated to Electron 22.3.2, which is based on Chromium 108.0.5359.215. Some of the issues listed below have been addressed and annotated.

## Determining the Electron Version in Use
Let's first look at what Electron version Discord is based on, because it's going to come in handy for the rest of the article.

We'll need to access the DevTools. The stable client blocks us from accessing the DevTools by default. We can set `"DANGEROUS_ENABLE_DEVTOOLS_ONLY_ENABLE_IF_YOU_KNOW_WHAT_YOURE_DOING": true` in `~/.config/discord/settings.json` (`~/.var/app/com.discordapp.Discord/config/discord/settings.json` for Flatpak) or use Discord Canary. In this example, I'm using Discord Canary from [flathub-beta] (`com.discordapp.DiscordCanary`).

We can look at the current version from the DevTools by pressing `Ctrl`+`Shift`+`I`, then going to the "Console" tab, and finally typing `navigator.userAgent` in the console. At the time of writing this article, the following is the output:
```
"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) discord/0.0.17 Chrome/91.0.4472.164 Electron/13.6.6 Safari/537.36"
```

The most important bit is at the end: `Chrome/91.0.4472.164 Electron/13.6.6`, where the Discord client uses Electron 13.6.6, which is based on Chromium 91.0.4472.164.

Now that we determined the Electron version, let's look at the problems that happen when using an outdated version of Electron.

## PipeWire Screen and Audio Sharing

***Note**: this wasn't addressed, as Discord manages audio and screen sharing unconventionally.*

X11 is slowly being replaced with Wayland in the hopes of improving stability, performance, better compatibility with hardware (touchscreen displays, trackpads, etc.). Since Wayland is architecturally different from X11, there are several implementation differences.

Since 2016, Fedora Linux switched to [Wayland on GNOME], later on [Plasma] and [NVIDIA] as well. Many toolkits, frameworks and browsers have slowly started supporting Wayland, including Electron.

For the longest time, the community has asked Discord to support PipeWire screen sharing for Wayland and sharing audio in calls.<sup>[[1]] [[2]] [[3]]</sup> Even the [topmost thread] on the "Voice and Video" category is about audio on Linux. While the Linux community is a minority, Linux specific requests are common and highly voted in the forums, but are unfortunately left ignored for a really long time. Theoretically, PipeWire screen sharing should be fixed by rebasing to a newer version of Electron and repatching/upstreaming patches.

Contrasting Electron versions, [Element], a Matrix client leveraging Electron, uses a more recent version of Electron. By following the same procedure to check the Electron version, we get: `[...] Chrome/98.0.4758.141 Electron/17.4.2 Safari/537.36`. In other words: it uses Electron 17.4.2, which is based on Chromium 98.0.4758.141. Just as a reminder, the Discord client uses Electron 13.6.6, which is based on Chromium 91.0.4472.164.

In my experience, Element works a lot better than Discord on Wayland. Newer Electron versions have many Wayland and PipeWire specific fixes. I can easily screen share with my friends with Element on Wayland and I can reliably use it on the Wayland backend without it [crashing](https://github.com/electron/electron/pull/33499). If we look at the changelogs between Electron 13.6.6 and 17.4.2, there are a few Wayland related changes. Some examples are:

> **[Electron 13.6.9](https://www.electronjs.org/releases/stable?version=13#13.6.9)**
>
> [...]
> - Fixed to ensure that chromium binds to wayland interface versions it supports
>
> [...]
>
> **[Electron 17.3.1](https://www.electronjs.org/releases/stable?version=17#17.3.1)**
>
> [...]
> - Fixed crash when running under Wayland caused by calling X11 functions.
>
> [...]

These were only Electron specific, i.e. non Chromium specific, improvements. To add more, there is a plethora of Wayland and PipeWire specific fixes and improvements between Chromium [91.0.4472.164 and 98.0.4758.141]. Beware that the list is *huge* and resource intensive. You can search for "wayland" and "pipewire" keywords and you should find relevant logs regarding Wayland and PipeWire respectively.

## XDG Portals

***Note**: this was addressed.*

[XDG portals] are processes that give sandboxed environments access to data that would otherwise be impossible, for example a file. Since last year, Electron supports the [FileChooser portal].<sup>[[Source](https://github.com/electron/electron/pull/19159#event-4543753441)]</sup> The FileChooser portal lets sandboxed environments open the host file picker so the user can access files outside of the sandbox, so long the application in question supports the portal.

Since Discord uses an old version of Electron, it sadly doesn't support the portal. Element in contrast does. This means that Discord opens the GTK file picker on virtually any desktop environment instead of the host file picker. If using Flatpak, then Discord cannot access files outside of the sandbox. Element on the other hand can, thanks to the portal.

## Addendum: Forced Manual Updates
The Discord client forces users to manually update on Linux. This is an inconvenient approach since we rely on package managers that automatically update packages. When Discord rolls out a major update, we are stuck in an [unusable state], where we are [forced to manually download] and install the new update, or wait for the packager to update the package to get it usable.

~~Having a flag to disable updates or an option in `settings.json` would be a great solution to disable forced manual updates. Firefox does something similar, where you can specify `"DisableAppUpdate": true` in `policies.json` to tell Firefox to not check for updates.~~

Update: you can indeed disable forced updates manually: [wiki.archlinux.org/title/Discord#Discord_asks_for_an_update_not_yet_available_in_the_repository](https://wiki.archlinux.org/title/Discord#Discord_asks_for_an_update_not_yet_available_in_the_repository). However, having it disabled by default would be a better approach.

This will make Discord a lot better on Linux because we won't run into this issue anymore.

## Blocker: Downstream Patches
The only blocker I can think of is the custom patches that Discord uses for Electron. Discord discloses the source code of its Electron fork. Since Discord uses Electron 13.6.6, I assume that the source code of the Electron version in use is the [`13-x-y`](https://web.archive.org/web/20220527021630/https://github.com/discord/electron/tree/13-x-y) branch, with [27 commits] ahead of upstream Electron. In theory, these 27 commits will need to be upstreamed or patched again against a recent version of Electron.

## Potential Security Risk

***Note**: this was addressed.*

Let's look at the security aspect of Discord.

In the [releases] page, at the time of writing this article, the Electron website states that Electron 13.6.6 was released [5 months ago] (January 04. 2022). Chrome 91.0.4472.164 was released [July last year]. Looking at all the later versions, there are dozens of security vulnerabilities left unpatched relative to Electron 13.6.6:

> **[Electron 13.6.8](https://www.electronjs.org/releases/stable?version=13#13.6.8)**
>
> - Backported fix for CVE-2021-38010. #31904
> - Backported fix for CVE-2021-38012. #32014
> - Backported fix for CVE-2021-38019. #32017
> - Backported fix for CVE-2021-4079. #32228
> - Backported fix for CVE-2021-4098. #32183
> - Backported fix for CVE-2021-4100. #32186
>
> **[Electron 13.6.7](https://www.electronjs.org/releases/stable?version=13#13.6.7)**
> - Backported fix for CVE-2021-38006. #32172
> - Backported fix for CVE-2021-38017. #32034
> - Backported fix for CVE-2021-38018. #32023
> - Backported fix for CVE-2021-4058. #32225
> - Backported fix for CVE-2021-4059. #32212
> - Backported fix for CVE-2021-4078. #32218

Do note that the page doesn't mention CVEs from 2022. Furthermore, it doesn't mention CVEs from Chromium [later than 91.0.4472.164](https://chromium.googlesource.com/chromium/src/+log/91.0.4472.164..main?pretty=fuller&n=10000) (Search for "cve"). Do note that this page is resource intensive as well.

To Discord's credit, they include the [safeStorage API],<sup>[[Source](https://web.archive.org/web/20220527184327/https://github.com/discord/electron/commit/a41ddf33b5e2fceab953369cd073575a93c27b70)]</sup> which prevents other applications from accessing Discord's data. However, there are still tens, twenties, perhaps even hundreds of CVEs left unpatched, and this increasingly worsens as time goes by because Electron is a fast moving framework; it is based on Chromium, which fast moving too as well, and is actively exploited by bad actors.

Rebasing to a recent version of Electron should fix most of these CVEs, and also protect Linux (and Windows and macOS) users more.

## Why It Actually Matters
The Steam Deck was announced late February, and very quickly grabbed Linux gamers' attention. And more importantly, the Deck also looked appealing to gamers who are unfamiliar with Linux. Users can install Discord on it and talk with their friends, peers, family and others. However, they unfortunately cannot stream audio because of the technical limits of Discord's Electron on Linux.

Based on [statistics], Discord install rate on Flathub nearly tripled, which may have a direct correlation to the release of the Steam Deck, and I personally think that this should be a little message to Discord that: "hey, the Linux desktop does indeed matter!" And these requests, in my opinion, should be a priority now that big companies are starting to focus on the Linux desktop.

## The Request to Discord
I would like to kindly ask Discord to please take our requests into consideration. I often have to open Firefox to screen share on Wayland, and I find it extremely frustrating and unintuitive. I also cannot afford to switch to Xorg and deal with trackpad gestures not working, screen tearing and the lack of security. In contrast, I can very easily screen share on Element because it uses a compatible version of Electron.

I would also like to ask Discord to maintain the [Discord Flatpak]. Since the Steam Deck uses Flathub by default, having official support on Flathub would be a great huge step for Discord and the Linux community. Having to maintain one package and deploying it on most distributions helps to have much wider official support in contrast to a `.deb`. Furthermore, using the same dependencies on all these distributions makes troubleshooting a lot easier too. Many companies, open source organizations and communities are showing interest in Flathub, such as Mozilla (Firefox), Telegram, OBS Studio, KeepassXC, and a lot more. The instructions of getting access to the manifest are available on the [Flathub wiki].

Even if Wayland and PipeWire aren't priorities, the benefits of rebasing to a newer version of Electron outweigh the "benefits" of using an ancient version of Electron. It is still really important as far as security goes. Electron and Chromium are huge and complex software; forking them for minor changes while actively ignoring to rebase is a huge problem.

At the end of the day, I am not a Discord developer, nor am I a contributor, and I am in no position to tell the developers what they should do or not do. It is up to the developers to prioritize whatever they deem important, but I personally believe that not focusing on rebasing to a recent version of Electron causes tons of problems. Of course, Discord is not the only developers following this practice here; there are tons of applications that don't utilize updated frameworks, toolkits, compositors, and more. Hopefully more and more developers can see the importance of keeping the base up-to-date.

---

Edit 1: mention that many organizations and communities are showing interest in Flathub (Credit to [u/Ok-Papaya-1730](https://www.reddit.com/r/linux/comments/v0g4ef/comment/iagchtk/?context=3))

Edit 2: mention that it's possible to enable DevTools in Discord stable; mention that the outdated version of Electron is also apparent on Windows and macOS (Credit to [Benedict Tomori](https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io/-/issues/43)); mention issues with forced manual updates (Credit to [@yukidream:matrix.org](https://matrix.to/#/@yukidream:matrix.org))

Edit 3: correct error about not being able to disable forced updates

Edit 4: Discord now uses a recent version of Electron (Credit to [0x5c](https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io/-/issues/66#note_1336648697) and [Oro](https://orowith2os.gitlab.io))

[topmost thread]: https://archive.ph/20mkq
[1]: https://support.discord.com/hc/en-us/community/posts/360068080771-Screen-sharing-issues-on-wayland
[2]: https://support.discord.com/hc/en-us/community/posts/4403787409047-Feature-Request-Linux-Wayland-screenshare-with-xdg-desktop-portal
[3]: https://support.discord.com/hc/en-us/community/posts/360050971374-Linux-Screen-Share-Sound-Support
[releases]: https://www.electronjs.org/releases/stable?version=13
[Wayland on GNOME]: https://fedoraproject.org/wiki/Changes/Wayland
[Plasma]: https://fedoraproject.org/wiki/Changes/WaylandByDefaultForPlasma
[NVIDIA]: https://fedoraproject.org/wiki/Changes/WaylandByDefaultOnNVIDIA
[Element]: https://element.io/
[Determining the Electron Version in Use]: #determining-the-electron-version-in-use
[91.0.4472.164 and 98.0.4758.141]: https://chromium.googlesource.com/chromium/src/+log/91.0.4472.164..98.0.4758.141?pretty=fuller&n=10000
[27 commits]: https://web.archive.org/web/20220522044845/https://github.com/electron/electron/compare/13-x-y...discord:13-x-y
[statistics]: https://web.archive.org/web/20220505182533/https://beta.flathub.org/apps/details/com.discordapp.Discord
[flathub-beta]: https://github.com/flathub/flathub/#using-the-flathub-repository
[XDG portals]: https://av.tib.eu/media/37941
[5 months ago]: https://www.electronjs.org/releases/stable?version=13#13.6.6
[July last year]: https://chromereleases.googleblog.com/2021/07/stable-channel-update-for-desktop.html
[safeStorage API]: https://www.electronjs.org/docs/latest/api/safe-storage
[FileChooser portal]: https://flatpak.github.io/xdg-desktop-portal/#gdbus-org.freedesktop.portal.FileChooser
[Flathub wiki]: https://github.com/flathub/flathub/wiki/App-Submission#someone-else-has-put-my-app-on-flathubwhat-do-i-do
[Discord Flatpak]: https://flathub.org/apps/details/com.discordapp.Discord
[forced to manually download]: https://itsfoss.com/install-discord-linux/#:~:text=the%20problem%20with%20this%20approach%20is%20that%20though%20you%E2%80%99ll%20have%20the%20latest%20discord%20version%2C%20it%20won%E2%80%99t%20be%20updated%20to%20a%20newer%20version%20automatically%20in%20the%20future.
[unusable state]: https://github.com/flathub/com.discordapp.Discord/issues/40
