---
layout: post
title: "Misconceptions About Immutable Distributions"
description: "I find that many Linux users have a misconception about immutable distributions without knowing what it actually *is*. There is a lot of misinformation and generalization in the Internet about immutable distributions being “locked down”, “inflexible”, etc., when we could argue the same with many traditional distributions.


In this article, we'll look at what makes an immutable distribution, the concept of an immutable distribution versus implementations, misconceptions about immutable distributions (both pro and con), and why they exist in the first place."
tags: ["FOSS", "Linux"]
toc: true
redirect_from: /2023/08/29/misconceptions-about-immutable-distributions.html
---

## Introduction

{{ page.description }}

## What Is an Immutable Distribution?

"Immutable" is a fancy way of saying unchangeable. So, an immutable distribution is essentially an unchangeable distribution, i.e. read-only. However, in most cases, the entirety of the distribution is *not* immutable. Some directories, like `/tmp`, are writable, as they need write capabilities to actually function. Some distributions may even have other directories, such as `/etc`, writable for extra functionality.

To redefine, **an immutable distribution is when, at a minimum, only the <u>core</u> of the distribution is read-only, and the rest is read-write**.

*Note: The more accurate terms are "reprovisionable" and "anti-hysteresis". However, since "immutable" is more commonly used, I'll use it instead throughout this article. Further reading: [“Immutable” → reprovisionable, anti-hysteresis] by Colin Walters.*

### Technical Definitions

There are a few technical definitions that should be understood before reading the rest of the article:

- **Rollback (rolling back)**: Undoing a change or update. For example, you upgrade your system to the latest version, which introduces bugs. You can then revert that update and go back to the version that worked well.
- **Reproducibility**: Delivering identical software regardless of the time or system used. Suppose you install a distribution on hundreds of laptops and desktops over the course of a week. The distribution you install on the first desktop (on the first day) will be *exactly* the same as the last laptop (on the last day). That is, all systems will be identical. You can think of it as a perfect clone.
- **Atomicity (atomic updates)**: Ensuring that a change (update, downgrade, etc.) is either fully applied or aborted. For example, your system shuts down in the middle of an upgrade due to a power outage. Instead of having a half-updated system, the update is fully aborted, meaning that your system was unchanged, just like before you started the update.

## Concept Versus Implementations

It's really easy to confuse the concept and the implementation when discussing immutability.

As we noted above, an immutable distribution is when the core of the distribution is read-only --- that's it. You (or someone else) decide to implement that concept as you see fit. You could have a locked down and inflexible system, like [GNOME OS],[^1] where there isn't even a system package manager; or you could have something that is designed to be flexible, like [NixOS], where you can change the desktop environment, the kernel, the boot loader, and so on.

You can think of an immutable distribution as any traditional distribution. Some traditional distributions are locked down and inflexible, such as [Fedora Workstation], where removing or even disabling SELinux can cause problems. Fedora Workstation also prevents you from accidentally removing core packages like `sudo`. Meanwhile, some traditional distributions are designed to be flexible, such as [Gentoo Linux], where you can change almost anything in your system.

## Misconceptions

There are several misconceptions both for and against immutable distributions that I would like to address.

I'm going to use pseudo quotes, which means I will be quoting and refining arguments I've interpreted from debates and media.

### "Needing to Reboot"

> Due to the read-only nature of immutable distributions, applying changes (updates, installs, etc.) absolutely requires a reboot.

The need of reboots varies per implementation. With [Endless OS], you must reboot to apply updates. On [Fedora Silverblue], you can run `sudo rpm-ostree apply-live` in the terminal to apply changes while the system is booted. On NixOS, you don't need to reboot at all.

Even then, rebooting your system after updating it is really important to keep it running optimally. As the article *[Restarting and Offline Updates]* explains: "*The Linux Kernel can change files without restarting, but the services or application using that file don’t have the same luxury. If a file being used by an application changes while the application is running then the application won’t know about the change. This can cause the application to no longer work the same way. As such, the adage that “Linux doesn’t need to restart to update” is a discredited meme. All Linux distributions should restart.*"

Many immutable distributions focus on reboots when performing system-level updates, because in many cases a reboot is crucial for stability. That's why Android, iOS, macOS, Windows, ChromeOS, etc. require a reboot.

### "Difficult to Use"

> Since immutable distributions are locked down, inflexible, and often need workarounds for various issues, they're more difficult to use.

Difficulty varies per implementation and users' priorities. *For me*, Fedora Silverblue is really easy to use, because it provides a great experience by default;[^2] I want something that is reliable, gets out of my way as much as possible, and follows closest with my political views (I value free software, inclusiveness in the community, and progressing the Linux desktop), which Fedora Silverblue is one of the few that provides.[^3] However, NixOS isn't for me, because I'm not a power user.

Endless OS is used by many educational institutions, because these institutions typically need a system that is simple, accessible and secure. On the contrary, NixOS is a powerful system typically used by power users and organizations that absolutely need the features it offers.

However, it's worth noting that, no matter the distribution, immutable or not, workarounds may be needed. Nothing is perfect, but there are many cases when external developers refuse to collaborate. For example, many drivers are unavailable in the Linux kernel, so we're sometimes forced to inconveniently install these drivers manually. While a distribution like Arch Linux has the AUR, most other traditional distributions do not have the luxury of installing obscure drivers. In short, this isn't a fundamental problem with immutable distributions; it's a problem that you have with every distribution.

### "Inflexible by Nature"

> Due to the read-only nature of immutable distributions, users cannot tinker with their system.

Again, this heavily depends on the implementation. If we look at traditional distributions, Ubuntu is not designed to be tinkered with, nor is Fedora Linux. As such, these distributions are prone to breaking after a significant amount of modification.

Meanwhile, NixOS, an immutable distribution, empowers the user and allows them to change many aspects of their system. Don't like GRUB? Use systemd-boot, rEFInd or something else. You want to add a Mandatory Access Control? Use SELinux. Don't like SELinux? use AppArmor or something else. Don't like using GNOME? Use Plasma, XFCE, MATE, Cinnamon, Sway or any desktop environment/window manager. Don't like the fact that you're using an LTS version of NixOS? Just [switch](https://nixos.wiki/wiki/Nix_channels) to an unstable or even a bleeding edge channel. What's that, you want to go back to LTS? You can. Want to mix and match packages from different channels? Go for it... You could view NixOS as Arch Linux on steroids.

Really, some traditional and immutable distributions are locked down and inflexible, meanwhile some others are flexible. This heavily varies per implementation.

### "Immutable Distributions Are More Secure"

> Immutable distributions provide better security, because \[...\]

I truly believe that statements like these create a false sense of security because "security" can be interpreted differently by different people. Allow me to identify the meaning<u>s</u> of "security" and address each of them:

#### "Security as a Safety Net"

> You can roll back or prevent damage in case an error occurs. For example, your system shuts down in the middle of an update due to a power outage. However, thanks to the system's atomicity, nothing was changed on the system, thereby allowing you to boot back into the system without a problem.

Atomicity could be replicated on traditional distributions if someone put in the effort.

> If the system updates but runs into problems, you can roll back from your bootloader.

Rollbacks can be implemented on traditional distributions. Linux Mint and openSUSE Tumbleweed/Leap are some implementors of this feature.

#### "Security as a Measure to Prevent Malicious Actors From Compromising Your System"

> Thanks to the read-only nature of immutable distributions, they prevent malicious actors from doing more damage than traditional distributions.

While immutable distributions might prevent malicious actors from doing certain things on your system, the bigger problem is that your personal data is at risk. Whether the actor can use `sudo` is less of an issue than the fact that your GPG keys, personal media and documents, or other sensitive data are easily accessible once compromised.

Whether your system is immutable or not, there are still really important steps you should be taking to make your system more secure. For example, you can use LUKS2 to encrypt your drive(s) to protect your data from physical access, and you can use Flatpak (with Flatseal) to make sure that your data is better protected from malicious/compromised apps. These steps are highly beneficial and should not be taken lightly!

#### "Security as a Measure to Prevent Users From Breaking Their Own Systems"

> Thanks to the read-only nature of immutable distributions, even the most destructive actions, like `sudo rm -rf --no-preserve-root /*` won't destroy the system.

While running such commands might prevent total destruction, they can still do enough damage to make the benefit meaningless. In this example, running `sudo rm -rf --no-preserve-root /*` will delete your `$HOME` (user) directory if you wait long enough, since the user directory is mutable; depending on the implementation, it could delete your boot partition as well. These destructive commands are usually destructive in other ways on immutable distributions, as the vulnerable bit is the mutable places. Only the immutable parts of the systems are unaffected, but the rest are just as vulnerable as any vulnerable place in traditional distributions.

### Immutability Is an Implementation Detail

Immutable and traditional distributions can literally share the same benefits and drawbacks, because, in reality, whether the core of the distribution is read-only or read-write is a minor detail. An immutable distribution could be as unstable as Manjaro and rely on distribution packages, meanwhile a traditional distribution could be as stable as Endless OS and fully embrace containers, such as Flatpak. A traditional distribution could be as locked down and inflexible as GNOME OS, meanwhile an immutable distribution could be as flexible as Gentoo Linux (theoretically).

Immutable distributions are different from each other, the same way traditional distributions are different from each other as well. For example, [Vanilla OS] utilizes [ABRoot], whose concept is inspired from Android's [A/B system updates]. Fedora Silverblue and Endless OS utilize [OSTree], which is similar to git in paradigm. [MicroOS] utilizes btrfs's features. NixOS does, uhh... yes.

While immutable and traditional distributions are presented as different in categories, they have very little difference in nature. It's best to treat immutable distributions like traditional distributions, rather than a whole different paradigm. It's up to the maintainers to decide what they want to do with their distribution.

## Immutability Relieves Maintenance Burden

Okay, as explored above, if the "benefits" are not benefits that are exclusive to immutable distributions and can be applied to traditional distributions, then why do immutable distributions exist in the first place? Why not just implement atomicity, reproducibility, and rollback functionalities to a traditional distribution?

Simply put, immutable distributions make it much easier for maintainers to focus on their high-level goals, because immutability eases the burden of maintenance.

The benefits are indirect for users, but direct for maintainers. Maintainers have fewer things to worry about than in traditional distributions, which allows them to spend their precious time and resources on the things they actually *want* to worry about, such as stability, reproducibility, providing a safety net, and so on. As a result, users get a more robust system.

This is why immutable distributions are generally considered "safer" than traditional distributions, even though the concept is not. So far, maintainers have put more effort into making their immutable distribution robust, as opposed to traditional distributions, which usually have to worry about the whole mutable aspect. This benefits immutable distributions greatly, because they don't have to worry about solving the problems that traditional distributions typically run into.

Similarly, many developers prefer to officially support Flatpak over traditional packaging formats, because they only have to worry about [one format]({% post_url 2022-08-29-traditional-packaging-is-not-suitable-for-modern-applications %}) as opposed to [hundreds](https://upload.wikimedia.org/wikipedia/commons/8/8c/Linux_Distribution_Timeline_Dec._2020.svg). As a result, they have more time and energy to focus on achieving their real goals, instead of focusing on tedious repetitive tasks and supporting a wider range of combinations (packaging format, dependencies version and patches, etc.).

All of these features (reproducibility, atomicity, and rollback) can also be implemented in traditional distributions. However, these features are typically treated as first-class features in immutable distributions, even though they are not mandated, whereas they are second-class features in most traditional distributions that support them. Immutability greatly reduces the maintenance overhead and leaves more room to improve the things maintainers want to improve. Immutable distributions have recently become popular, and are already used and popular in educational and personal systems.

## Conclusion

To summarize, an immutable distribution is when the core of the distribution is read-only. Anyone is free to grab that concept and implement a distribution with it. The "benefits" and "inconveniences" that we tend to see with immutable distributions could be applied on traditional distributions, but immutable distributions tend to be easier to accomplish the high-level goals, because they are easier to maintain.

In the end, immutability should be seen as an implementation detail and philosophical/political decision, rather than a purely technical one, because traditional and immutable distributions can literally share the same benefits and drawbacks.

[Restarting and Offline Updates]: https://fedoramagazine.org/offline-updates-and-fedora-35

**Footnotes**

[^1]: GNOME OS is used for development purposes, so it doesn't need a package manager.

[^2]: Except its [downsides]({% post_url 2022-12-06-where-fedora-linux-could-improve %}).

[^3]: I'm currently looking into [Vanilla OS](https://vanillaos.org) and think about switching to it once [2.0 is released](https://vanillaos.org/blog/article/2023-03-07/vanilla-os-20-orchid---initial-work).

[ABRoot]: https://github.com/Vanilla-OS/ABRoot
[A/B system updates]: https://source.android.com/docs/core/ota/ab
[OSTree]: https://en.wikipedia.org/wiki/OSTree
[btrfs snapshots]: https://get.opensuse.org/is/microos
[GNOME OS]: https://os.gnome.org
[NixOS]: https://nixos.org
[Fedora Workstation]: https://fedoraproject.org/workstation
[Gentoo Linux]: https://www.gentoo.org
[Vanilla OS]: https://vanillaos.org
[Fedora Silverblue]: https://fedoraproject.org/silverblue
[Endless OS]: https://www.endlessos.org
[MicroOS]: https://microos.opensuse.org
[“Immutable” → reprovisionable, anti-hysteresis]: https://blog.verbum.org/2020/08/22/immutable-%e2%86%92-reprovisionable-anti-hysteresis/
