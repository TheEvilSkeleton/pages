---
layout: post
title: "Coming Out as Trans"
toc: true
additional_classes: 🏳️‍⚧️
trigger: "

- This post contains mentions of gender dysphoria, discrimination against the LGBTQ+ community, depression, and suicidal thoughts.

- If at any moment you feel overwhelmed, and need to talk to someone, here is a list of mental health resources compiled by the Wikimedia Foundation:<br><https://w.wiki/3$YP>
"
disclaimer: "I'm still recovering from everything I've been going through in the last few months (frankly, from the last decade of untreated gender dysphoria). Due to depression and anxiety, I may be misremembering some dates and lengths of time."
description: "I am publicly announcing that I am transgender. I have experienced gender dysphoria for almost ten years, but I have found a label that I feel comfortable with now.\n\nIn this article, I'm going to go over:

- The environment I've lived for the last ten years

- My gender identity

- How to seek help

- Tips for those exploring themselves or seeking to understand trans individuals"
---

## Introduction

{{ page.description }}

## Vocabularies

Before I delve into my personal experience, allow me to define several key terms:
- **Sex**: *Biological* characteristics of males and females.
- **Gender**: *Social* characteristics of men and women, such as [norms, roles, and behaviors].
- **Gender identity**: How you personally view your own gender.
- **Gender dysphoria**: Sense of unease due to a mismatch between gender identity and sex assigned at birth.
- **Transgender (trans)**: The gender identity differs from the sex assigned at birth. If someone's gender identity is woman but their sex assigned at birth is male, then they are generally considered a trans person.
- **Cisgender (cis)**: The opposite of transgender; when the gender identity fits with the sex assigned at birth.
<!-- - **Transsexual**: Changing from one sex to another, albeit it's seen as outdated to some. -->
- **Non-binary**: Anything that is not exclusively male or female. Imagine if male and female were binary numbers: male is 0 and female is 1. Anything that is *not* 0 or 1 is considered non-binary. If I see myself as the number 0.5 or 2, then I'm non-binary. Someone who considers themself to be between a man and woman would be between 0 and 1 (e.g. 0.5).
- **Agender**: Under the umbrella of non-binary; it essentially means non-gendered (lack of gender identity) or gender neutral. Whichever definition applies varies from person to person. It's also worth noting that many agender people don't consider themselves trans.
- **Label**: Portraying which group you belong to, such as "non-binary", "transfemme" (trans (binary and non-binary) people who are feminine), etc.

## Backstory

Allow me to share a little backstory. I come from a neighborhood where being anti-LGBTQ+ was considered "normal" a decade ago. This outlook was quite common in the schools I attended, but I wouldn't be surprised if a considerably significant portion of the people around here are still anti-LGBTQ+ today. Many individuals, including former friends and teachers, have expressed their opposition to LGBTQ+ in the past, which influenced my own view against the LGBTQ+ community at the time.

Due to my previous experiences and the environment I live(d) in, I tried really hard to avoid thinking about my sexuality and gender identity for almost a decade. Every time I thought about my sexuality and gender identity, I'd do whatever I could to distract myself. I kept forcing myself to be as masculine as possible. However, since we humans have a limit, I eventually reached a limit to the amount of thoughts I could suppress.

I always struggled with communicating and almost always felt lonely whenever I was around the majority of people, so I pretended to be "normal" and hid my true feelings. About 5 years ago, I began to spend most of my time online. I met people who are just like me, many of which I'm still friends with 3-4 years later. At the time, despite my strong biases against LGBTQ+ from my surroundings, I naturally felt more comfortable within the community, far more than I did outside. I was able to express myself more freely and have people actually understand me. It was the only time I didn't feel the need to act masculine. However, despite all this, I was still in the mindset of suppressing my feelings. ~~Truly an [egg irl] moment~~

Eventually, I was unable hold my thoughts anymore, and everything exploded. All I could think about for a few months was my gender identity: my biases between my childhood environment often clashed with me questioning my own identity, and whether I really saw myself as a man. I just had these recurring thoughts and a lot of anxiety about where I'm getting these thoughts from, and why.

Since then, my work performance got exponentially worse by the week. I quickly lost interest in my hobbies, and began to distance myself from communities and friends. I often lashed out on people because my mental health was getting worse. My sleep quality was also getting worse, which only worsened the situation. On top of that, I still had to hide my feelings, which continued to exhaust me. All I could think about for months was my gender identity.

After I slowly became comfortable with and accepting of my gender identity, I started having suicidal thoughts on a daily basis, which I was able to endure... until I reached a breaking point once again. I was having suicidal thoughts on a bi-hourly basis. It escalated to hourly, and finally almost 24/7. I obviously couldn't work anymore, nor could I do my hobbies. I needed to hide my pain because of my social anxiety. I also didn't have the courage to call the suicide hotline either. What happened was that I talked to many people, some of whom have encouraged and even helped me seek professional help.

However, that was all in the past. I feel much better and more comfortable with myself and the people I opened up to, and now I'm confident enough to share it publicly 😊

## Coming Out‎ ‎🏳️‍⚧️

I identify as agender. My pronouns are any/all --- I'll accept any pronouns. I don't think I have a preference, so feel free to call me as whatever you want; whatever you think fits me best :)

I'm happy with agender because I feel disconnected from my own masculinity. I don't think I belong at either end of the spectrum (or even in between), so I'm pretty happy that there is something that best describes me.

## Why the Need to Come Out Publicly?

So... why come out publicly? Why am I making a big deal out of this?

Simply put, I am really proud and relieved for discovering myself. For so long, I tried to suppress my thoughts and force myself to be someone I was fundamentally not. While that never worked, I explored myself instead and discovered that I'm trans. However, I also wrote this article to explain how much it affected me for living in a transphobic environment, even before I discovered myself.

For me, displaying my gender identity is like displaying a username or profile picture. We choose a username and profile picture when possible to give a glimpse of who we are.

I chose "TheEvilSkeleton" as my username because I used to play Minecraft regularly when I was 10 years old. While I don't play Minecraft anymore, it helped me discover my passion: creating and improving things and working together --- that's why I'm a programmer and contribute to software. I chose Chrome-chan as my profile picture because I think she is cute and I like cute things :3. I highly value my username and profile picture, the same way I now value my gender identity.

## Am I Doing Better?

While I'm doing much better than before, I did go through a depressive episode that I'm still recovering from at the time of writing, and I'm still processing the discovery because of my childhood environment, but I certainly feel much better after discovering myself and coming out.

However, coming out won't magically heal the trauma I've experienced throughout my childhood environment. It won't make everyone around me accept who I am, or even make them feel comfortable around me. It won't drop the amount of harassment I receive online to zero --- if anything, I write this with the expectation that I will be harassed and discriminated against more than ever.

There will be new challenges that I will have to face, but I still have to deal with the trauma, and I will have to deal with possible trauma in the future. The best thing I can do is train myself to be mentally resilient. I certainly feel much better coming out, but I'm still worried about the future. I sometimes wish I wasn't trans, because I'm genuinely terrified about the things people have gone through in the past, and are still going through right now.

I know I'm going to have to fight for *my* life now that I've come out publicly, because apparently the right to live as yourself is still controversial in 2024.

## Seeking Help

Of course, I wasn't alone in my journey. What helped me get through it was talking to my friends and seeking help in other places. I came out to several of my friends in private. They were supportive and listened to me vent; they reassured me that there's nothing wrong with me, and congratulated me for discovering myself and coming out.

Some of my friends encouraged and helped me seek professional help at local clinics for my depression. I have gained more confidence in myself; I am now capable to call clinics by myself, even when I'm nervous. If these suicidal thoughts escalate again, I will finally have the courage to call the suicide hotline.

**If you're feeling anxious about something, don't hesitate to talk to your friends about it.** Unless you know that they'll take it the wrong way and/or are currently dealing with personal issues, they will be more than happy to help.

I have messaged so many people in private and felt much better after talking. I've never felt so comforted by friends who try their best to be there for me. Some friends have listened without saying anything, while some others have shared their experiences with me. Both were extremely valuable to me, because sometimes I just want (and need) to be heard and understood.

<!-- ## Tips and Resources -->

<!-- I think the hardest part about anything transgender is getting started. I've put together a non-exhaustive list of things you could follow to respect and understand trans people better. Many of these tips helped me understand my trans friends when I was a "cis male": -->
<!-- - [Transgender Glossary: Terms You Can Learn] and [LGBTQIA+ Glossary of Terms for Teaching in Health Care] from Yale Medicine. These are glossaries of transgender-related terms, including (offensive) terms you should avoid. -->
<!-- - Try to respect people's pronouns. It's perfectly fine to make mistakes. What really matters is that you do your best, and not in bad faith. I got people's pronouns wrong a lot before I discovered myself. The majority of trans people were forgiving and respectfully corrected me because they knew my intentions. As long as you try, the majority of us will appreciate your effort. Keep in mind that some people may feel uncomfortable if they are referred to by the wrong pronoun, accidentally or not, which could prompt their gender dysphoria and cause them to be rude or defensive. If this ever happens, please apologize, and try to reassure them that you don't mean any harm. -->
<!-- - You, as a cis person, will not win everyone's heart for various reasons. You will have to accept that. There are many trans people who have unfortunately had bad experiences with cis people due to constant exposure to transphobia. Your best bet is to not engage and leave them alone. It's sad, but it is what it is. -->
<!-- - Try to respect people's boundaries. This is something I still struggle with, but I'm getting better at it every day. Here are some tips I can share with you: -->
<!--   1. If you are discussing something that is personal to trans people, ask if they would like to continue. It's really important to keep in mind that many trans people are very sensitive about their past and anything that might seem threatening, as there may be a lot of trauma involved that they don't want to be reminded of. A lot of the things they have to explain are emotionally taxing, and having to repeat it to every person can really exhaust them emotionally and mentally. -->
<!--   2. If you ever have a trans friend who trusts your good intentions, I suggest you politely ask questions within their boundaries. In many cases, you won't be able to do simple searches with search engines, so your second best resource is the people around you (physically and digitally). I learned most of my knowledge by asking questions to other trans people. Even if you've offended another trans person, I would suggest asking your trans friend if they're willing to give you feedback on your behavior and/or why they were offended. -->
<!--   3. When discussing a sensitive issue with a trans person, I suggest avoiding [tone policing]. Tone policing is arguing/focusing on the tone rather than the argument or point. This has been the hardest for me to recognize because I often focus on tone. As explained above, many trans people have to explain their trauma and discomfort over and over again. In addition, many trans people are [neurodivergent], which may make it hard for them to convey tone in a conventional style. As a result, they may sound rude when explaining their point. However, if things escalate, try to take a step back and remember where they're coming from. Repeat the previous steps. -->

<!-- Interestingly, however, my social skills have improved drastically as a result of these steps, because I became better at understanding people's feelings. Unfortunately, I have upset some people in the past and will continue to upset people in the future when I don't mean to. I sincerely apologize to all the people I have offended in the past and for ruining our friendships. -->

<!-- --- -->

If you're currently trying to suppress your thoughts and really trying to force yourself into the gender you were assigned at birth, like I was, the best advice I can give you is to give yourself time to explore yourself. It's perfectly fine to acknowledge that you're not cisgender (that is, if you're not). You might want to ask your trans friends to help you explore yourself. From experience, it's not worth forcing yourself to be someone you're not.

## Closing Thoughts

I feel relieved about coming out, but to be honest, I'm still really worried about the future of my mental health. I really hope that everything will work out and that I'll be more mentally resilient.

I'm really happy that I had the courage to take the first steps, to go to clinics, to talk to people, to open up publicly. It's been really difficult for me to write and publish the article. I'm really grateful to have wonderful friends, and legitimately, I couldn't ask for better friends.

[norms, roles, and behaviors]: https://en.wikipedia.org/wiki/Gender_role
[Transgender Glossary: Terms You Can Learn]: https://www.yalemedicine.org/news/transgender-guide-terms-you-can-learn
[LGBTQIA+ Glossary of Terms for Teaching in Health Care]: https://medicine.yale.edu/lgbtqi/curriculum/2021-07-04-lgbtqia_glossary_417482_37430_v3.pdf
[tone policing]: https://en.wikipedia.org/wiki/Tone_policing
[neurodivergent]: https://en.wikipedia.org/wiki/Neurodiversity
[egg irl]: https://www.reddit.com/r/egg_irl
