---
layout: post
title: "Libadwaita: Splitting GTK and Design Language"
toc: true
description: "
Recently, the Linux Mint Blog published [Monthly News – April 2024](https://blog.linuxmint.com/?p=4675), which goes into detail about wanting to fork and maintain older GNOME apps in collaboration with other GTK-based desktop environments.


Despite the good intentions of the author, Clem, many readers interpreted this as an attack against GNOME. Specifically: [GTK](https://gtk.org), [libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita), the relationship between them, and their relevance to any desktop environment or desktop operating system. Unfortunately, many of these readers seem to have a lot of difficulty understanding what GTK *is* trying to be, and how libadwaita helps.


In this article, we'll look at the history of why and how libadwaita was born, the differences between GTK 4 and libadwaita in terms of scope of support, their relevance to each desktop environment and desktop operating system, and the state of GTK 4 today.
"
---

## Introduction

{{ page.description }}

## What Is GTK?

First of all, what is GTK? GTK is a cross-platform widget toolkit from the GNOME Project, which means it provides interactive elements that developers can use to build their apps.

The latest major release of GTK is 4, which brings performance improvements over GTK 3. GTK 4 also removes several widgets that were part of the GNOME design language, which became a controversy. In the context of application design, a *design language* is the visual characteristics that are communicated to the user. Fonts, colors, shapes, forms, layouts, writing styles, spacing, etc. are all elements of the design <span class="no-wrap">language.<sup>[(Source)](https://www.smashingmagazine.com/2020/03/visual-design-language-building-blocks/)</sup></span>

## Unnecessary Unification of the Toolkit and Design Language

In general, cross-platform toolkits tend to provide general-purpose/standard widgets, typically with a non-opinionated styling, i.e. widgets and design patterns that are used consistently across different operating systems (OSes) and desktop environments.

However, GTK had the unique case of bundling GNOME's design language into GTK, which made it far from generic, leading to problems of different lexicons, mainly philosophical and technical problems.

### Clash of Philosophies

When we look at apps made for the GNOME desktop (will be referred to as "GNOME apps") as opposed to non-GNOME apps, we notice that they're distinctive: GNOME apps tend to have [hamburger buttons], [header bars], larger buttons, larger padding and margins, etc., while most non-GNOME apps tend to be more compact, use [menu bars], [standard title bars], and many other design metaphors that may not be used in GNOME apps.

This is because, from a design philosophy standpoint, GNOME's design patterns tend to go in a different direction than most apps. As a brand and product, GNOME has a design language it adheres to, which is accompanied by the GNOME [Human Interface Guidelines] (HIG).

As a result, GTK and GNOME's design language clashed together. Instead of being as general-purpose as possible, GTK as a cross-platform toolkit contained an entire design language intended to be used only by a specific desktop, thus defeating the purpose of a cross-platform toolkit.

*For more information on GNOME's design philosophy, see "[What is GNOME's Philosophy?]".*

### Inefficient Diversion of Resources

The unnecessary unification of the toolkit and design language also divided a significant amount of effort and maintenance: Instead of focusing solely on the general-purpose widgets that could be used across all desktop OSes and environments, much of the focus was on the widgets that were intended to conform to the GNOME HIG. Many of the general-purpose widgets also included features and functionality that were only relevant to the GNOME desktop, making them less general-purpose.

Thus, the general-purpose widgets were being implemented and improved slowly, and the large codebase also made the GNOME widgets and design language difficult to maintain, change, and adapt. In other words, almost everything was hindered by the lack of independence on both sides.

### Libhandy: the Predecessor

Because of the technical bottlenecks caused by the philosophical decisions, [libhandy] was created [in 2017](https://source.puri.sm/Librem5/libhandy/-/commit/d7b066dcac396ec65904a632fb91f63abe5b42cc), with the first experimental version released in [2018](https://source.puri.sm/Librem5/libhandy/-/commit/6b34695abbf64281f5a2879bd82fa5e8469ba7da). As described on the website, libhandy is a collection of "\[b\]uilding blocks for modern adaptive GNOME applications." In other words, libhandy provides additional widgets that can be used by GNOME apps, especially those that use GTK 3. For example, [Boxes uses libhandy], and many GNOME apps that used to use GTK 3 also used libhandy.

However, some of the problems remained: Since libhandy was relatively new at the time, most GNOME widgets were still part of GTK 3, which continued to suffer from the consequences of merging the toolkit and design language. Furthermore, GTK 4 was released at the end of December 2020 --- after libhandy. Since libhandy was created before the initial release of GTK 4, it made little sense to fully address these issues in GTK 3, especially when doing so would have caused major breakages and inconveniences for GTK, libhandy, and app developers. As such, it wasn't worth the effort.

With these issues in mind, the best course of action was to introduce all these major changes and breakages in GTK 4, use libhandy as an experiment and to gain experience, and properly address these issues in a successor.

## Libadwaita: the Successor

Because of all the above problems, libadwaita was created: libhandy's successor that will accompany GTK 4.

GTK 4 was initially released in [December 2020](https://blog.gtk.org/2020/12/16/gtk-4-0/), and libadwaita was released one year later, in [December 2021](https://blogs.gnome.org/alicem/2021/12/31/libadwaita-1-0/). With the experience gained from libhandy, libadwaita managed to become extensible and easy to maintain.

Libadwaita is a platform library accompanying GTK 4. A *platform library* is a library used to complement a specific platform. In the case of libadwaita, the platform it targets is the GNOME desktop.

### Porting Widgets to Libadwaita

Some GNOME widgets from GTK 3 (or earlier versions of GTK 4) were removed or deprecated in GTK 4 and were reimplemented in / transferred to libadwaita, for example:
- ~~[GtkDialog]~~ → [AdwDialog](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Dialog.html)[^1]
- ~~[GtkInfoBar]~~ → [AdwBanner]

These aforementioned widgets only benefited GNOME apps, as they were strictly designed to provide widgets that conformed to the GNOME HIG. Non-GNOME apps usually didn't use these widgets, so they were practically irrelevant to everyone else.

In addition, libadwaita introduced several widgets as counterparts to GTK 4 to comply with the HIG:
- [GtkHeaderBar] → [AdwHeaderBar]
- [GtkAlertDialog] → [AdwAlertDialog]
- [GtkAboutDialog] → [AdwAboutDialog]

Similarly, these aforementioned GTK 4 (the ones starting with `Gtk`) widgets are not designed to comply with the GNOME HIG. Since GTK 4 widgets are supposed to be general-purpose, they should not be platform-specific; the HIG no longer has any influence on GTK, only on the development of libadwaita.

### Scope of Support

The main difference between GTK 4 and libadwaita is the scope of support, specifically the priorities in terms of the GNOME desktop, and desktop environment and OS support. While most resources are dedicated to GNOME desktop integration, GTK 4 is not nearly as focused on the GNOME desktop as libadwaita. GTK 4, while opinionated, still tries to get closer to the traditional desktop metaphor by providing these general-purpose widgets, while libadwaita provides custom widgets to conform to the GNOME HIG.

Since libadwaita is only made for the GNOME desktop, and the GNOME desktop is primarily officially supported on Linux, libadwaita thus primarily supports Linux. In contrast, GTK is officially supported on all major operating systems (Windows, macOS, Linux). However, since GTK 4 is mostly developed by GNOME developers, it works best on Linux and <span class="no-wrap">GNOME --- hence</span> "opinionated".

## State of GTK 4 Today

Thanks to the removal of GNOME widgets from GTK 4, GTK developers can continue to work on general-purpose widgets, without being influenced or restricted in any way by the GNOME HIG. Developers of cross-platform GTK 3 apps that rely exclusively on general-purpose widgets can be more confident that GTK 4 won't remove these widgets, and hopefully enjoy the benefits that GTK 4 offers.

At the time of writing, there are several cross-platform apps that have either successfully ported to GTK 4, or are currently in the process of doing so. To name a few: [Freeciv gtk4 client](https://flathub.org/apps/org.freeciv.gtk4), [HandBrake](https://handbrake.fr), [Inkscape](https://inkscape.org), [Transmission](https://transmissionbt.com), and [PulseAudio Volume Control](https://freedesktop.org/software/pulseaudio/pavucontrol/). The [LibreOffice](https://libreoffice.org) developers are working on the GTK 4 port, with the [`gtk4` VCL plugin](https://wiki.documentfoundation.org/Development/GTK4) option enabled. For example, the [`libreoffice-fresh`](https://archlinux.org/packages/extra/x86_64/libreoffice-fresh/) package from Arch Linux has it [enabled](https://gitlab.archlinux.org/archlinux/packaging/packages/libreoffice-fresh/-/blob/d5fc44b4659c0512fea49277d04ca505bd4e5299/PKGBUILD#L202).

Here are screenshots of the aforementioned apps:

{% include image.html
url="/assets/Articles/libadwaita-splitting-gtk-and-design-language/freeciv-gtk4.webp"
caption="*Freeciv gtk4 client* in the game view, displaying a title bar, a custom background, a menu bar, a tab view with the Chat tab selected, an entry, and a few buttons."
drop_shadow=false %}

{% include image.html
url="/assets/Articles/libadwaita-splitting-gtk-and-design-language/handbrake-gtk4.webp"
caption="*HandBrake* in the main view, displaying a title bar, a menu bar, a horizontal toolbar below it with custom buttons, entries, popover buttons, a tab view with the Summary tab selected, containing a popover button and several checkboxes."
drop_shadow=false %}

{% include image.html
url="/assets/Articles/libadwaita-splitting-gtk-and-design-language/inkscape-gtk4.webp"
caption="Development version of *Inkscape* in the main view, displaying a title bar, a menu bar, a horizontal toolbar below, vertical toolbars on the left and right, a canvas grid on the center left, a tab view on the center right with the Display Properties tab selected, and a toolbar at the bottom."
drop_shadow=false %}

{% include image.html
url="/assets/Articles/libadwaita-splitting-gtk-and-design-language/libreoffice-gtk4.webp"
caption="*LibreOffice Writer* with the experimental `gtk4` VCL plugin in workspace view, displaying a title bar, a menu bar, two horizontal toolbars below, a vertical toolbar on the right, a workspace grid in the middle with selected text, and a status bar at the bottom."
drop_shadow=false %}

{% include image.html
url="/assets/Articles/libadwaita-splitting-gtk-and-design-language/transmission-gtk4.webp"
caption="*Transmission* in the main view, displaying a title bar, a menu bar, a horizontal toolbar, a filter bar, an empty field in the center of the view, and a status bar at the bottom."
drop_shadow=false %}

{% include image.html
url="/assets/Articles/libadwaita-splitting-gtk-and-design-language/pavucontrol-gtk4.webp"
caption="*PulseAudio Volume Control* in the Output Devices view, displaying a title bar, a tab section, a list of output devices, and a bottom bar with a combo box."
drop_shadow=false %}

## A GNOME App Remains a GNOME App, Unless Otherwise Stated

*This is a counter-response to Thom Holwerda's [response to this article](https://www.osnews.com/story/139865/libadwaita-splitting-gtk-and-design-language/).*

An app targeting a specific platform will typically run best on that platform and will naturally struggle to integrate with other platforms. Whether the libraries change over time or stay the same forever, if the developers are invested in the platform they are targeting, the app will follow the direction of the platform and continue to struggle to integrate with other platforms. At best, it will integrate in other platforms by accident.

In this case, developers who have and will continue to target the GNOME desktop will actively adapt their apps to follow the GNOME philosophy, for better or worse. Hamburger buttons, header bars, typography, and distinct design patterns were already present a decade ago (2014).<sup>[(Source)](https://web.archive.org/web/20141116174926/https://developer.gnome.org/hig/stable/)</sup> Since other platforms were (and still are) adhering to different design languages, with or without libhandy/libadwaita, the GTK 3 apps targeting GNOME were already distinguishable a decade ago. Custom solutions such as theming were (and still are) inadequate, as there was (and still is) no <span class="no-wrap">🪄 magical 🪄</span> solution that converts GNOME's design patterns into their platform-agnostic counterparts.

Whether the design language is part of the toolkit or a separate library has no effect on integration, because GNOME apps already looked really different long before libhandy was created, and non-GNOME apps already looked "out of place" in GNOME as well. Apps targeting a specific platform that unintentionally integrate with other platforms will eventually stop integrating with other platforms as the target platform progresses and apps adapt. In rare cases, developers may decide to no longer adhere to the GNOME HIG.

## Alternate Platforms

While libadwaita is the most popular and widely used platform library that accompanies GTK 4, there are several alternatives to libadwaita:

- [Granite] is developed and maintained by [elementary, Inc.], and focuses on [elementary OS] and the Pantheon desktop. Apps that use Granite can be found in the [elementary AppCenter].
- [Libhelium] is developed and maintained by [Fyra Labs], and focuses on [tauOS]. Apps using libhelium can be found in the ["libhelium" topics on GitHub].

There are also several alternatives to libhandy:

- [libxapp] is developed and maintained by [Linux Mint], and focuses on multiple GTK desktop environments, such as Cinnamon, MATE, and Xfce.
- [libxfce4ui] is developed and maintained by [Xfce], and focuses on Xfce.

Just like libadwaita and libhandy, these platform libraries offer custom widgets and styling that differ from GTK and are built for their respective platforms, so it's important to realize that **GTK is meant to be built with a complementary platform library that extends its functionality <u>when targeting a specific platform</u>**.

Similarly, [Kirigami] from KDE accompanies Qt to build Plasma apps. [MauiKit] from the Maui Project (another KDE project) also accompanies Qt, but targets [Nitrux]. [Libcosmic] by System76 accompanies [iced] to build COSMIC apps.

## Conclusion

A cross-platform toolkit should primarily provide general-purpose widgets. Third parties should be able to extend the toolkit as they see fit through a platform library if they want to target a specific platform.

As we've seen throughout the philosophical and technical issues with GTK, a lot of effort has gone into moving GNOME widgets from GTK 4 to libadwaita. GTK 4 will continue to provide these general-purpose widgets for apps intended to run on any desktop or OS, while platform libraries such as libadwaita, Granite and libhelium provide styling and custom widgets that respect their respective platforms.

Libadwaita is targeted exclusively at the GNOME ecosystem, courtesy of the GNOME HIG. Apps built with libadwaita are intended to run best on GNOME, while GTK 4 apps that don't come with a platform library are intended to run everywhere.

---

[^1]: The core functionality of GtkDialog, i.e. creating dialogs, has been moved to [GtkWindow].

[menu bar]: https://docs.gtk.org/gtk4/class.PopoverMenuBar.html
[menu bars]: https://en.wikipedia.org/wiki/Menu_bar
[standard title bars]: https://learn.microsoft.com/en-us/windows/apps/design/basics/titlebar-design#standard-design
[title bar]: https://docs.gtk.org/gtk4/property.Window.titlebar.html
[Human Interface Guidelines]: https://developer.gnome.org/hig
[hamburger buttons]: https://en.wikipedia.org/wiki/Hamburger_button
[What is GNOME's Philosophy?]: {% post_url 2023-04-02-what-is-gnomes-philosophy %}
[libhandy]: https://gnome.pages.gitlab.gnome.org/libhandy
[Boxes uses libhandy]: https://gitlab.gnome.org/GNOME/gnome-boxes/-/blob/f253b5c9bc908c2c53ad7676394c25fd60d05410/src/meson.build#L105
[Granite]: https://valadoc.org/granite-7/index.htm
[Libhelium]: https://docs.developers.tauos.co/libhelium/libhelium-1
[elementary, Inc.]: https://elementary.io/brand
[elementary OS]: https://elementary.io
[tauOS]: https://tauos.co
[Fyra Labs]: https://fyralabs.com
[GtkInfoBar]: https://docs.gtk.org/gtk4/class.InfoBar.html
[AdwBanner]: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Banner.html
[GtkAlertDialog]: https://docs.gtk.org/gtk4/class.AlertDialog.html
[AdwAlertDialog]: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AlertDialog.html
[GtkAboutDialog]: https://docs.gtk.org/gtk4/class.AboutDialog.html
[AdwAboutDialog]: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AboutDialog.html
[GtkHeaderBar]: https://docs.gtk.org/gtk4/class.HeaderBar.html
[AdwHeaderBar]: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.HeaderBar.html
[GtkDialog]: https://docs.gtk.org/gtk4/class.Dialog.html
[Kirigami]: https://develop.kde.org/frameworks/kirigami//
[MauiKit]: https://mauikit.org/
[elementary AppCenter]: https://appcenter.elementary.io/
["libhelium" topics on GitHub]: https://github.com/topics/libhelium
[iced]: https://iced.rs/
[Libcosmic]: https://pop-os.github.io/libcosmic/cosmic/
[Nitrux]: https://nxos.org/
[header bars]: https://developer.gnome.org/hig/patterns/containers/header-bars.html
[GtkWindow]: https://docs.gtk.org/gtk4/class.Window.html
[libxapp]: https://github.com/linuxmint/xapp
[libxfce4ui]: https://docs.xfce.org/xfce/libxfce4ui/start
[Linux Mint]: https://www.linuxmint.com/
[Xfce]: https://xfce.org/
