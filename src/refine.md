---
title: "Refine"
description: "Tweak various aspects of GNOME"
layout: marketing
embed_image: "/assets/Refine/embed.webp"
icon: "/assets/refine-icon.svg"
accent_hue: 265

---

<div class="metadata">
<a href='https://flathub.org/apps/page.tesk.Refine'><img class="download-link" height="64" alt="Get it on Flathub" src="https://flathub.org/api/badge?locale=en"/></a>

<div class="badges-list">
<span><a href="https://flathub.org/apps/page.tesk.Refine"><img src="https://img.shields.io/flathub/downloads/page.tesk.Refine?label=Installs" alt="Installs"></a></span>
<span><a href="https://gitlab.gnome.org/TheEvilSkeleton/Refine"><img src="https://img.shields.io/gitlab/v/tag/TheEvilSkeleton/Refine?gitlab_url=https%3A%2F%2Fgitlab.gnome.org&amp;label=Refine" alt="GitLab tag (latest by SemVer)"></a></span>
</div>

</div>

{% include image.html
url="/assets/Refine/embed.webp"
url-light="/assets/Refine/embed-light.webp"
drop_shadow=false
%}

## Links

- [Source Code](https://gitlab.gnome.org/TheEvilSkeleton/Refine)
- [Chat](https://matrix.to/#/#refine:gnome.org)

## License

Copyright © 2024 Hari Rana (TheEvilSkeleton)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


[GNOME GitLab]: https://gitlab.gnome.org/World/Upscaler
[GitHub]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[Upscaler Contributors]: https://gitlab.gnome.org/World/Upscaler/-/graphs/main
[Upscayl NCNN]: https://github.com/upscayl/upscayl-ncnn
[Real-ESRGAN ncnn Vulkan]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[GTK4]: https://docs.gtk.org/gtk4
[libadwaita]: https://gnome.pages.gitlab.gnome.org/libadwaita
[PyGObject]: https://pygobject.readthedocs.io/en/latest
[free and open-source software]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[GNOME desktop]: https://www.gnome.org
