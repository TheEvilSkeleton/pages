---
title: "Upscaler"
description: "Upscale and enhance images"
layout: marketing
embed_image: "/assets/Upscaler/embed.webp"
icon: "/assets/upscaler-icon.svg"
accent_hue: 350

presentation:
- title: "Private & Secure"
  image: "/assets/Upscaler/upscaler-is-safe.webp"
  image-light: "/assets/Upscaler/upscaler-is-safe-light.webp"
  description: "Upscaler is fully restricted and cannot access your files, devices, or even the internet."
  drop_shadow: false
- title: "Customizable"
  image: "/assets/Upscaler/options-page.webp"
  image-light: "/assets/Upscaler/options-page-light.webp"
  description: "You can change your settings to suit your needs."
  drop_shadow: false
  source: "https://www.flickr.com/photos/taylar/8512601698"
- title: "Offline"
  image: "/assets/Upscaler/queue-page.webp"
  image-light: "/assets/Upscaler/queue-page-light.webp"
  description: "There's absolutely no internet access. Everything is run locally on your personal machine."
  drop_shadow: false
- title: "Restore Images"
  image: "/assets/Upscaler/upscaled.webp"
  description: "With just a few clicks, you can turn blurry images into clearer and high-quality images."
  source: "https://pbs.twimg.com/media/FYH38wSaQAECj__?format=png&name=4096x4096"

requirements:
- key: "OS"
  value: "Linux-based operating system"
- key: "Graphics"
  value: "[Vulkan](https://vulkan.gpuinfo.org/listdevices.php?platform=linux)-capable GPU"

project_links:
- title: "Upscaler"
  links:
  - text: "Source code"
    url: "https://gitlab.gnome.org/World/Upscaler"
  - text: "Download app"
    url: "https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler"
  - text: "Chat"
    url: "https://matrix.to/#/%23Upscaler%3Agnome.org"
- title: "Upscayl NCNN"
  links:
  - text: "Source code"
    url: "https://github.com/upscayl/upscayl-ncnn"
- title: "Real-ESRGAN ncnn Vulkan"
  links:
  - text: "Source code"
    url: "https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan"
---

<div class="metadata">
<a href="https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler"><img class="download-link" height="64" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.png"/></a>

<div class="badges-list">
<span><a href="https://matrix.to/#/%23Upscaler%3Agnome.org"><img src="https://img.shields.io/matrix/upscaler:matrix.org" alt="Matrix chat"></a></span>
<span><a href="https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler"><img src="https://img.shields.io/flathub/downloads/io.gitlab.theevilskeleton.Upscaler?label=Installs" alt="Installs"></a></span>
<span><a href="https://gitlab.gnome.org/World/Upscaler"><img src="https://img.shields.io/gitlab/v/tag/World/Upscaler?gitlab_url=https%3A%2F%2Fgitlab.gnome.org&amp;label=Upscaler" alt="GitLab tag (latest by SemVer)"></a></span>
</div>

</div>

{% include image.html
url="/assets/Upscaler/embed.webp"
url-light="/assets/Upscaler/embed-light.webp"
drop_shadow=false
%}

## Features

{% include presentation.html rows=page.presentation %}

## System Requirements

{% include kvl.html rows=page.requirements %}

## Technical Details

Upscaler is a graphical interface for, and powered by [Upscayl NCNN], a program that aims to develop practical algorithms for general image/video restoration.

Upscaler is written in Python, using [PyGObject], [GTK4], and [libadwaita]. It is targeted for the [GNOME desktop].

## Links

{% include bl.html columns=page.project_links %}

## Licenses

### Upscaler

Copyright © 2022 Upscaler Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

### Upscayl NCNN

Copyright © 2024 Upscayl

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>. 

### Real-ESRGAN ncnn Vulkan

Copyright © 2021 Xintao Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[GNOME GitLab]: https://gitlab.gnome.org/World/Upscaler
[GitHub]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[Upscaler Contributors]: https://gitlab.gnome.org/World/Upscaler/-/graphs/main
[Upscayl NCNN]: https://github.com/upscayl/upscayl-ncnn
[Real-ESRGAN ncnn Vulkan]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[GTK4]: https://docs.gtk.org/gtk4
[libadwaita]: https://gnome.pages.gitlab.gnome.org/libadwaita
[PyGObject]: https://pygobject.readthedocs.io/en/latest
[free and open-source software]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[GNOME desktop]: https://www.gnome.org
