---
title: "Looking for Work"
description: "Hello, my name is Hari Rana. I'm looking for contract or part-time remote employment. I live in Quebec, Canada. I would prefer to work on free and open source software. I have a high school diploma (Diplôme d'études secondaire in Québec). A low salary is fine as long as the work I do is for social purposes."
layout: general
---

{{ page.description }}

I have experience in:
- HTML, CSS, Jekyll, Python and C (open to learn more languages)
- Shell scripting
- Linux
- Docker/Podman (open to learn more container technologies)
- Flatpak packaging (open to learn more packaging formats)
- GTK+libadwaita
- GitHub/GitLab (open to learn more services)
- Quality Assurance (testing software, submitting feedback through reviews or opening issues/tickets)
- Technical Writing/Documentation
- Writing articles
- Researching

Certifications and pursues:
- [CS50's Introduction to Computer Science](https://courses.edx.org/certificates/3c726c5051fb4b9b84a0402770bff4d1) (Harvard University / edX)
- [CS50's Introduction to Programming with Python](https://courses.edx.org/certificates/911f1d1fb34849af84b997cf58ec2856) (Harvard University / edX)
- Currently pursuing [CS50's Introduction to Artificial Intelligence with Python](https://www.edx.org/course/cs50s-introduction-to-artificial-intelligence-with-python)
- Interested in Rust
- Interested in design and accessibility

Feel free to explore my website for insights on who I am and what I do. If you want to contact me, please look at [Contact](/about#contact).
